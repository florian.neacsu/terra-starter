variable "vpc_cidr" {
  description = "Range of IPv4 addresses for the VPC"
}

variable "name" {
  description = "Env name"
}
