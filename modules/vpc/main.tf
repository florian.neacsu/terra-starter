resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support = true
  instance_tenancy = "default"

  tags = {
    Name = "${var.name}_vpc"
    Environment = var.name
    Description = "${var.name} Virtual Private Cloud"
  }
}

resource "aws_internet_gateway" "vpc" {
  vpc_id = aws_vpc.vpc.id

  tags =  {
    Name = "${aws_vpc.vpc.id}_igw"
    Environment = var.name
    Description = "${var.name} Internet Gateway"
  }
}
