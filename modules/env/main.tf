module "vpc" {
  source = "../vpc/"

  name = var.name
  vpc_cidr = var.vpc_cidr
}

module "public_subnet" {
  source = "../subnet/"

  name = "public_subnet_${var.name}"
  cidrs = var.cidr_public_main
  vpc_id = module.vpc.id
  azs = var.azs
}

module "private_data_subnet" {
  source = "../subnet/"

  name = "private_data_subnet_${var.name}"
  cidrs = var.cidr_private_data
  vpc_id = module.vpc.id
  azs = var.azs
}

module "private_ec2_subnet" {
  source = "../subnet/"

  name = "private_ec2_subnet_${var.name}"
  cidrs = var.cidr_private_ec2
  vpc_id = module.vpc.id
  azs = var.azs
}

resource "aws_route" "public_igw_route" {
  count = length(var.cidr_public_main)

  route_table_id = element(module.public_subnet.route_table_ids, count.index)
  gateway_id = module.vpc.igw_id
  destination_cidr_block = "0.0.0.0/0"
}
