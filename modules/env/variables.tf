variable "vpc_cidr" {
  description = "Range of IPv4 addresses for the VPC"
}

variable "cidr_public_main" {
  description = "CIDR block for public subnet"
  type = list(string)
}

variable "cidr_private_data" {
  description = "CIDR block for private data subnet"
  type = list(string)
}

variable "cidr_private_ec2" {
  description = "CIDR block for private compute subnet"
  type = list(string)
}

variable "name" {
  description = "Env name"
}

variable "azs" {
  description = "A list of availability zones"
  default     = []
}
