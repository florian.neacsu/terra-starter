output "vpc_id" {
  value = module.vpc.id
}

output "vpc_cidr" {
  value = module.vpc.cidr_block
}

output "vpc_main_route_table_id" {
  value = module.vpc.main_route_table_id
}

output "public_subnet_route_table_ids" {
  value = module.public_subnet.route_table_ids
}

output "private_data_subnet_route_table_ids" {
  value = module.private_data_subnet.route_table_ids
}

output "private_ec2_subnet_route_table_ids" {
  value = module.private_ec2_subnet.route_table_ids
}

output "public_subnet_ids" {
  value = module.public_subnet.subnet_ids
}

output "private_data_subnet_ids" {
  value = module.private_data_subnet.subnet_ids
}

output "private_ec2_subnet_ids" {
  value = module.private_ec2_subnet.subnet_ids
}
