variable "name" {
  description = "Service name"
}

variable "env" {
  description = "Env name"
}

variable "cluster_id" {
  description = "ECS Cluster id"
}

variable "desired_count" {
  description = "Number of replicas"
}

variable "task_definition_json" {
  description = "Json task definition"
}