resource "aws_ecs_service" "service" {
  name            = var.name
  cluster         = var.cluster_id
  task_definition = aws_ecs_task_definition.service.arn

  desired_count = var.desired_count

  deployment_maximum_percent         = 100
  deployment_minimum_healthy_percent = 0
}

resource "aws_cloudwatch_log_group" "service" {
  name              = "hello_world"
  retention_in_days = 1
}

resource "aws_ecs_task_definition" "service" {
  family = "service"

  container_definitions = var.task_definition_json

  tags = {
    Name = var.name
    Environment = var.env
    Description = "Service ${var.name} in ${var.env}"
  }
}
