resource "aws_ecs_cluster" "ecs" {
  name = var.ecs_name

  capacity_providers = [aws_ecs_capacity_provider.ecs_provider.name]

  default_capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.ecs_provider.name # "EC2"
    weight            = 1
    base              = 1
  }

  tags = {
    Name = var.ecs_name
    Environment = var.name
    Description = "${var.name} ECS Cluster"
  }
}

resource "aws_ecs_capacity_provider" "ecs_provider" {
  name = "prov-${var.ecs_name}"

  auto_scaling_group_provider {
    auto_scaling_group_arn = module.aws_asg.this_autoscaling_group_arn
  }

}

module "ecs-instance-profile" {
  source = "./iam/"
  name = var.ecs_name
}

module "aws_asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 3.0"

  # Launch Configuration
  name = "${var.ecs_name}_${var.name}"

  lc_name = "${var.ecs_name}_${var.name}"
  recreate_asg_when_lc_changes = true
  image_id = var.ecs_ami_id
  instance_type = var.ecs_instance_type
  security_groups = var.ecs_security_groups
  iam_instance_profile = module.ecs-instance-profile.iam_ecs_instance_profile_id
  user_data = var.user_data

  root_block_device = [
    {
      volume_size = var.ecs_ebs_volume_size
      volume_type = "gp2"
    },
  ]

  #Auto scaling group
  asg_name = "${var.ecs_name}_${var.name}"
  vpc_zone_identifier = var.ecs_subnets
  health_check_type = "EC2"
  min_size = var.min_size
  max_size = var.max_size
  desired_capacity = var.desired_capacity
  wait_for_capacity_timeout = 0

  tags = [
    {
      key                 = "Environment"
      value               = var.name
      propagate_at_launch = true
    },
    {
      key                 = "Name"
      value               = "${var.name} ECS ASG"
      propagate_at_launch = true
    },
    {
      key                 = "Description"
      value               = "${var.name} ECS ASG"
      propagate_at_launch = true
    }
  ]
}
