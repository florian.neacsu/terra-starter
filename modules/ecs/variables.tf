variable "ecs_name" {
  description = "ECS cluster name"
}

variable "name" {
  description = "Env name"
}

variable "vpc_id" {
  description = "VPC Id"
}

variable "ecs_ami_id" {
  description = "Amazon machine image"
}

variable "ecs_instance_type" {
  description = "EC2 instance type"
}

variable "ecs_security_groups" {
  description = "ECS security groups"

  type = list(string)
}

variable "ecs_subnets" {
  description = "Subnets where to launch the container nodes"
}

variable "ecs_ebs_volume_size" {
  description = "EBS volume size"
}

variable "min_size" {
  description = "The minimum size of the auto scale group"
  type        = string
}

variable "desired_capacity" {
  description = "The number of Amazon EC2 instances that should be running in the group"
  type        = string
}

# Autoscaling group
variable "max_size" {
  description = "The maximum size of the auto scale group"
  type        = string
}


variable "user_data" {
  description = "The user data to provide when launching the instance. Do not pass gzip-compressed data via this argument; see user_data_base64 instead."
  type        = string
  default     = null
}
