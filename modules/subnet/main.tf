resource "aws_subnet" "subnet" {
  count = length(var.cidrs)

  vpc_id = var.vpc_id
  cidr_block = element(var.cidrs, count.index)
  availability_zone = element(var.azs, count.index)

  tags = {
    Name = "${var.name}.${element(var.azs, count.index)}-${element(var.cidrs, count.index)}"
  }
}

resource "aws_route_table" "subnet" {
  count = length(var.cidrs)

  vpc_id = var.vpc_id

  tags = {
    Name = "${var.name}.${element(var.azs, count.index)}-${element(var.cidrs, count.index)}"
  }
}

resource "aws_route_table_association" "subnet" {
  count = length(var.cidrs)

  route_table_id = element(aws_route_table.subnet.*.id, count.index)
  subnet_id = element(aws_subnet.subnet.*.id, count.index)
}