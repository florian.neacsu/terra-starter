terraform {
  backend "s3" {
    bucket = "terraform-state-dev-necorp"
    key = "dev/eu-west-2/core/terraform-s3-backend/terraform.tfstate"
    region = "eu-west-2"
    profile = "dev"

    dynamodb_table = "terraform-up-and-running-locks"
    encrypt        = true
  }
}
