module "ecs" {
  source = "../../../../modules/ecs"

  ecs_name = local.ecs_name
  name = local.env

  vpc_id = data.terraform_remote_state.network.outputs.vpc_id
  ecs_ami_id = data.aws_ami.ami_ubuntu_18_04_ecs.id
  ecs_instance_type = "t2.micro"
  ecs_ebs_volume_size = 10
  min_size = "1"
  max_size = "2"
  desired_capacity = "1"
  ecs_subnets = data.terraform_remote_state.network.outputs.public_subnet_ids
  ecs_security_groups = [aws_security_group.ecs_security_group.id]
}

resource "aws_security_group" "ecs_security_group" {
  name = "${local.ecs_name}_ecs_container_instance"
  description = "SG managed by Terraform"
  vpc_id = data.terraform_remote_state.network.outputs.vpc_id

  tags = {
    Name = "${local.ecs_name} ECS SG"
    Environment = local.env
    Description = "${local.ecs_name} ECS Egress SG"
  }
}

resource "aws_security_group_rule" "ecs_security_group_rule_egress" {
  from_port = 0
  to_port = 65535
  protocol = "tcp"
  type = "egress"
  security_group_id = aws_security_group.ecs_security_group.id
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "ecs_security_group_rule_ingress" {
  from_port = 80
  to_port = 80
  protocol = "tcp"
  type = "ingress"
  security_group_id = aws_security_group.ecs_security_group.id
  cidr_blocks = ["0.0.0.0/0"]
}