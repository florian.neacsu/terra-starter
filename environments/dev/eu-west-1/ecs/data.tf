data "terraform_remote_state" "network" {
  backend = "remote"

  config = {
    organization = "necorp"
    workspaces = {
      name = "terra-starter"
    }
  }
}

data "aws_ami" "ami_ubuntu_18_04_ecs" {
  most_recent = true
  owners = ["099720109477"] # Canonical

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
}


data "template_file" "user_data" {
  template = file("${path.module}/templates/user-data.sh")

  vars = {
    cluster_name = local.ecs_name
  }
}