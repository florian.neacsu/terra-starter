#----- ECS  Services--------
module "ngix" {
  source = "../../../../modules/ecs_service"

  name = "nginx"
  env = local.env
  cluster_id = data.terraform_remote_state.ecs.outputs.ecs_cluster_id
  desired_count = "1"
  task_definition_json = file("${path.module}/templates/nginx.json")
}
