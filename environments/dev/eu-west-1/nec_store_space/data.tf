data "terraform_remote_state" "ecs" {
  backend = "remote"

  config = {
    organization = "necorp"
    workspaces = {
      name = "terra-starter-ecs"
    }
  }
}