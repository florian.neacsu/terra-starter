module "dev-env-eu-west-1" {
  source = "../../../../modules/env/"

  name = "dev"
  vpc_cidr = "10.0.0.0/16"
  cidr_public_main = ["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24"]
  cidr_private_ec2 = ["10.0.4.0/23", "10.0.6.0/23", "10.0.8.0/23"]
  cidr_private_data = ["10.0.10.0/24", "10.0.11.0/24", "10.0.12.0/24"]
  azs = data.aws_availability_zones.available.names
}
