output "vpc_id" {
  value = module.dev-env-eu-west-1.vpc_id
}

output "vpc_cidr" {
  value = module.dev-env-eu-west-1.vpc_cidr
}

output "public_subnet_route_table_ids" {
  value = module.dev-env-eu-west-1.public_subnet_route_table_ids
}

output "private_data_subnet_route_table_ids" {
  value = module.dev-env-eu-west-1.private_data_subnet_route_table_ids
}

output "private_ec2_subnet_route_table_ids" {
  value = module.dev-env-eu-west-1.private_ec2_subnet_route_table_ids
}

output "public_subnet_ids" {
  value = module.dev-env-eu-west-1.public_subnet_ids
}

output "private_data_subnet_ids" {
  value = module.dev-env-eu-west-1.private_data_subnet_ids
}

output "private_ec2_subnet_ids" {
  value = module.dev-env-eu-west-1.private_ec2_subnet_ids
}
