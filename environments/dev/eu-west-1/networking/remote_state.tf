terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "necorp"

    workspaces {
      name = "terra-starter"
    }
  }
}
